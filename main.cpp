#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include "tree.h"
#include <vector>



using namespace std;

int main()
{
	Tree<int> mytree;
	vector<int>data;
	srand(time(NULL));
	int num;
	for (int i = 0; i < 500; i++) {
		num = rand() % 500;
		mytree.insert(num);//insert 500 and random 0-499 
		data.push_back(num);//push 500 and random 0-499 
	}

	mytree.inorder();//sort and show
	int num2;
	mytree.clear();//clear 
	mytree.balance(data, 0, data.size() - 1);  //call function balance 
	cout << "Height is " << mytree.maxDepth() << endl;  //show max height
	cout << "Input Number to search : ";
	cin >> num2;
	if (mytree.search(num2)) {  // function search
		cout << "Found " << num2 << "!!!\n";
	}
	else {
		cout << "Don't Found " << num2 << "!!!\n";
	}
	mytree.DeleteSearch(499);  // function delete 
	mytree.inorder();  //sort 
	system("pause");
	return 0;
}